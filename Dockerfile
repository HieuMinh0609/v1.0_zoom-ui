FROM node:12.13.0 as build
WORKDIR /usr/src/docker-react-sample
COPY package*.json ./
RUN npm install
COPY . .
EXPOSE 9999
ENTRYPOINT ["npm","run", "start-prod-window"]