import './App.css';
import {BrowserRouter as Router,Switch,Route,Link} from "react-router-dom";
import Room from './component/room';
declare var ZoomMtg
//const uat_url = "http://localhost:8082";
// const uat_url = "http://elearning-uat.tmgs.vn";

// ZoomMtg.setZoomJSLib('https://source.zoom.us/1.8.6/lib', '/av');
ZoomMtg.setZoomJSLib('https://source.zoom.us/2.0.1/lib', '/av');
ZoomMtg.preLoadWasm();
ZoomMtg.prepareJssdk();

function App() {

 

  return (
    <Router >
  
    <Switch>
          <Route path="/live"  >
            <Room />
          </Route>
        
        </Switch>
    </Router>
  );
 
}

export default App;
