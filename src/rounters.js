import React from 'react';
import App from './App';
const routes = [  
    {
        path : '/live',
        exact : true ,
        main : () => <App/>
    } 
];


export default routes;