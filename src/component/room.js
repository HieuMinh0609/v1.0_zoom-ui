import React, { useState, useEffect } from 'react';
import '../App.css';
declare var ZoomMtg
//const uat_url = "http://localhost:8082";
const url_app = process.env.REACT_APP_API_ENDPOINT;

ZoomMtg.setZoomJSLib('https://source.zoom.us/2.0.1/lib', '/av');

ZoomMtg.preLoadWasm();
ZoomMtg.prepareJssdk()
const Room = () =>{


    useEffect(() => {
      document.getElementById("joinMeeting").click()
    })

    var leaveUrl =  process.env.REACT_APP_API_ENDPOINT;
    // var userName = 'React'
    // var userEmail = ''
    // var passWord = ''
    console.log(process.env.REACT_APP_API_ENDPOINT);
    function getSignature(e) {
      e.preventDefault();
      let  id_meet=parseQuery().id_meet;
      let  eln_token=parseQuery().eln_token?parseQuery().eln_token:null;
      let  eln_token_admin=parseQuery().eln_token_admin?parseQuery().eln_token_admin:null;
      
      let urlUser ="/api/v2/user/detail";
      let urlLive="/api/live/"+id_meet+"/detail";
      let token = eln_token
      if(eln_token_admin){
        urlUser = "/api/admin/user/detail";
        urlLive = "/api/admin/live/detail/"+id_meet;
        token = eln_token_admin;
      }

      Promise.all([
        fetch(url_app+urlUser, {
          method: 'GET',
          headers: { 
            'Content-Type': 'application/json',
            'Authorization': "Bearer "+token
         },
      }),
      fetch(url_app+urlLive, {
        method: 'GET',
        headers: { 
          'Content-Type': 'application/json',
          'Authorization': "Bearer "+token
       },
       })
      ]).then(function (responses) {
        // Get a JSON object from each of the responses
        return Promise.all(responses.map(function (response) {
          return response.json();
        }));
      }).then(function (data) {
        //  if(data[0].code === "200" && data[1].code === "200"){
          let  dataUser=data[0].data;
          let dataLive =data[1].data;
          console.log(dataLive);
          startMeeting(dataLive.generateSignature,dataUser,dataLive)
      //  }else{
      //   alert("Có lỗi sảy ra");
      //  }
       
       
      }).catch(function (error) {
        // if there's an error, log it
        alert("Có lỗi sảy ra,kiểm tra đường truyền của bạn");
        console.log(error);
      });
  
  
  
    }
  
    // function getSignatureAffter(dataUser,dataLive){
    //   fetch(signatureEndpoint, {
    //     method: 'POST',
    //     headers: { 'Content-Type': 'application/json' },
    //     body: JSON.stringify({
    //       meetingNumber: dataLive.idMeet+"",
    //       role: parseInt(dataUser.username===dataLive.createdBy?1:0)
    //     })
    //   }).then(res => res.json())
    //   .then(response => {
  
    //   }).catch(error => {
    //     console.error(error)
    //   })
  
    // }
    
  
  function parseQuery() {
      return (function () {
        var href = window.location.href;
        var queryString = href.substr(href.indexOf("?"));
        var query = {};
        var pairs = (queryString[0] === "?"
          ? queryString.substr(1)
          : queryString
        ).split("&");
        for (var i = 0; i < pairs.length; i += 1) {
          var pair = pairs[i].split("=");
          query[decodeURIComponent(pair[0])] = decodeURIComponent(pair[1] || "");
        }
        return query;
      })();
    }
  
    function startMeeting(signature,dataUser,dataLive) {
      document.getElementById('zmmtg-root').style.display = 'block'
      console.log(signature);
      console.log(dataUser);
      console.log(dataLive);
      ZoomMtg.init({
        leaveUrl: leaveUrl,
        isSupportAV: true,
        meetingInfo: [
          'topic',
          'mn',
          'host',
          'participant',
          'enctype',
          'dc',
          'report'
        ],
        success: (success) => {
          console.log(success)
          ZoomMtg.i18n.load("vi-VN");
          ZoomMtg.i18n.reload("vi-VN");
          ZoomMtg.reRender({ lang: "vi-VN" });
          ZoomMtg.join({
            signature: signature,
            meetingNumber: dataLive.idMeet,
            userName: dataUser.fullName,
            apiKey: dataLive.apiKey,
            userEmail: dataUser.email,
            passWord: dataLive.password,
            success: (success) => {
              console.log(success)
            },
            error: (error) => {
              console.log(error)
            }
          })
        },
        error: (error) => {
          console.log(error)
        }
      })
    }

     return (
    <div className="App">
      <main>
        <h1>Phòng học trực tuyến</h1>
        <p>{process.env.REACT_APP_API_ENDPOINT}</p>
        <button id = "joinMeeting" onClick={getSignature}>Tham gia</button>
      </main>
    </div>
  );
}
export default Room;